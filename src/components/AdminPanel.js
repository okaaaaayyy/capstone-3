import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function AdminPanel(props) {

	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	// Functions to handle opening and closing modals
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const openEdit = (productId) => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})			

		setShowEdit(true)
	}


	const closeEdit = () => {
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}

	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added"
				})

				fetchData()
				closeAdd()

				setName("")
				setDescription("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"					
				})
				fetchData()
			}
		})
	}

	const updateProduct = (e) => {
		e.preventDefault()

	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated"
				})

				fetchData()
				closeEdit()

				setName("")
				setDescription("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"					
				})
				fetchData()
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			fetchData()
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: `Product successfully ${bool}`
				})
			}else{
				fetchData()
				Swal.fire({
					title: "Something went wrong",
					icon:"error",
					text: "Please try again"
				})
			}
		})
	}	

	useEffect(() => {
		const products = productsProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>

						}
					</td>
				</tr>
			)
		})

		setProductsArr(products)
	}, [productsProp])

	return(
		<>
			<div className="my-4 text-center">
				<h2>Admin Dashboard</h2>
				<Button variant="primary" onClick={openAdd}>Add New Product</Button>
			</div>

		{/*Course info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productsArr}
				</tbody>
			</Table>

		{/*Add Course Modal*/}
		<Modal show={showAdd} onHide={closeAdd}>
			 <Form onSubmit={e => addProduct(e)}>
			 	<Modal.Header closeButton>
			 		<Modal.Title>Add Product</Modal.Title>
			 	</Modal.Header>
			 	<Modal.Body>
			 		<Form.Group controlId="productName">
			 			<Form.Label>Name</Form.Label>
			 			<Form.Control 
			 				value={name}
			 				onChange={e => setName(e.target.value)}
			 				type="text"
			 				required
			 			/>
			 		</Form.Group>

			 		<Form.Group controlId="productDescription">
			 			<Form.Label>Description</Form.Label>
			 			<Form.Control 
			 				value={description}
			 				onChange={e => setDescription(e.target.value)}
			 				type="text"
			 				required
			 			/>
			 		</Form.Group>

			 		<Form.Group controlId="productPrice">
			 			<Form.Label>Price</Form.Label>
			 			<Form.Control 
			 				value={price}
			 				onChange={e => setPrice(e.target.value)}
			 				required
			 				type="number"
			 				/>
			 		</Form.Group>
			 	</Modal.Body>
			 	<Modal.Footer>
			 		<Button variant="secondary" onClick={closeAdd}>Close</Button>
			 		<Button variant="success" type="submit">Submit</Button>
			 	</Modal.Footer>
			 </Form>
		</Modal>


		{/*Edit Course Modal*/}
		<Modal show={showEdit} onHide={closeEdit}>
			 <Form onSubmit={e => updateProduct(e)}>
			 	<Modal.Header closeButton>
			 		<Modal.Title>Update Product</Modal.Title>
			 	</Modal.Header>
			 	<Modal.Body>
			 		<Form.Group controlId="productName">
			 			<Form.Label>Name</Form.Label>
			 			<Form.Control 
			 				value={name}
			 				onChange={e => setName(e.target.value)}
			 				type="text"
			 				required
			 			/>
			 		</Form.Group>

			 		<Form.Group controlId="productDescription">
			 			<Form.Label>Description</Form.Label>
			 			<Form.Control 
			 				value={description}
			 				onChange={e => setDescription(e.target.value)}
			 				type="text"
			 				required
			 			/>
			 		</Form.Group>

			 		<Form.Group controlId="productPrice">
			 			<Form.Label>Price</Form.Label>
			 			<Form.Control 
			 				value={price}
			 				onChange={e => setPrice(e.target.value)}
			 				required
			 				type="number"
			 				/>
			 		</Form.Group>
			 	</Modal.Body>
			 	<Modal.Footer>
			 		<Button variant="secondary" onClick={closeEdit}>Close</Button>
			 		<Button variant="success" type="submit">Submit</Button>
			 	</Modal.Footer>
			 </Form>
		</Modal>

	</>

	)
}