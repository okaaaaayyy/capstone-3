import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home(){
	const data = {
		title: "Title",
		content: "Content",
		destination: "/products",
		label: "label"
	}

	return(
		<>
			<Banner bannerProps={data}/>
			<Highlights/>
		</>

	)
}