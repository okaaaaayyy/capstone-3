import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()

	const loginUser = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem("token", data.accessToken)

				fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
				
					Swal.fire({
						title: "Login successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					})
					history.push("/products")
				})

			}else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}
		})
	}

	useEffect(() => {

		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [email, password])

	if(user.id !== null){

	Swal.fire({
		title: "Error",
		icon: "error",
		text: "You are already logged in"
	})

		return <Redirect to="/"/>
	}

	return(

	<>
		<h3 className="text-center my-3">Login</h3>
		<Form onSubmit={e => loginUser(e)} className="my-3">

			<Form.Group controlId="email">
				<Form.Label>Email</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password" className="mb-4">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="secondary" id="submitBtn" disabled>Submit</Button>
			}

		</Form>
	</>
	)
}
