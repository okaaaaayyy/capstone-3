import { useState, useEffect, useContext } from 'react';
import UserView from "../components/UserView"
import AdminPanel from "../components/AdminPanel"
import UserContext from "../UserContext"

export default function Courses() {

	const { user } = useContext(UserContext)

	const [coursesData, setCoursesData] = useState([])


	// console.log(courseData)

	const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/products`)
		.then(res => res.json())
		.then(data => {
			setCoursesData(data)

		})
	}

	useEffect(() => {
		fetchData()
	}, [])


	return(
		user.isAdmin === true ?
			<AdminPanel coursesProp={coursesData} fetchData={fetchData}/>
			:
			<UserView coursesProp={coursesData}/>
		
	)
	
}